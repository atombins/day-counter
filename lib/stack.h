#ifndef STACK_H_INCLUDED
#define STACK_H_INCLUDED

#include <stdlib.h>

struct _stack_elem
{
    void* data;
    struct _stack_elem* next;
};


typedef struct _stack
{
    struct _stack_elem* top;
} stack;

/**
 * @brief Function will set default values for empty stack
 * 
 * @param to_init allocated memory to init
 */
void stack_init(stack* to_init);

/**
 * @brief Funcion will add one record on top of the stack
 * 
 * @param target the stack, where data will be pushed
 * @param data pointer to data object
 * @return int 0 on success, other number otherwise
 */
int stack_push(stack* target, void* data);

/**
 * @brief Function returns number of items on stack.
 * 
 * @param target target stack
 * @return unsigned number of items on stack
 */
unsigned stack_get_size(stack* target);

/**
 * @brief Function will return pointer on field of data pointers. This field must be freed.
 * 
 * @param target stack from witch will be field created
 * @return void** resulting field, number of elements is equal to number of elements on stack during call
 */
void** stack_get_as_field(stack* target);

/**
 * @brief Function will free stack from top. Note data and stack* will not be freed.
 * 
 * @param to_free 
 */
void stack_free(stack* to_free);

#endif // STACK_H_INCLUDED