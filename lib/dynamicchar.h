#ifndef DYNAMICCHAR_H_INCLUDED
#define DYNAMICCHAR_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>

#define DCHAR_MALLOC_SIZE 128

typedef struct {
  char *text;
  unsigned int size;
} dchar;

dchar *DCHAR_Init();
int DCHAR_Add(dchar *target, char c);
char DCHAR_PopLast(dchar *target);
void DCHAR_Print(dchar *target);
void DCHAR_Clear(dchar *target);
void DCHAR_Destroy(dchar *target);
char *DCHAR_DestroyAndGetText(dchar *target);

#endif // DYNAMICCHAR_H_INCLUDED
