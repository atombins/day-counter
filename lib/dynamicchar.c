#include "dynamicchar.h"

dchar *DCHAR_Init()
{
	dchar *ret = (dchar *)malloc(sizeof(dchar));
	if (ret == NULL)
		return NULL;
	ret->text = NULL;
	ret->size = 0;
	return ret;
}

int DCHAR_Add(dchar *target, char c)
{
	if (target->size % DCHAR_MALLOC_SIZE == 0)
	{
		char *tmp = (char *)realloc(target->text, target->size + DCHAR_MALLOC_SIZE * sizeof(char));
		if (tmp == NULL)
		return 1;
		target->text = tmp;
	}
	target->text[target->size++] = c;
	return 0;
}

char DCHAR_PopLast(dchar *target)
{
	target->size -= 1;
	char ret = target->text[target->size];
	if ((target->size) % DCHAR_MALLOC_SIZE == 0)
	{
		char *tmp = (char *)realloc(target->text, target->size * sizeof(char) + DCHAR_MALLOC_SIZE * sizeof(char));
		if (tmp == NULL)
		return 1;
		target->text = tmp;
	}
	return ret;
}

void DCHAR_Print(dchar *target)
{
	for (unsigned i = 0; i < target->size; i++)
	{
		putchar(target->text[i]);
	}
}

void DCHAR_Clear(dchar *target)
{
	if (target == NULL)
		return;
	free(target->text);
	target->text = NULL;
	target->size = 0;
}

void DCHAR_Destroy(dchar *target)
{
    if (target->text != NULL)
    {
      free(target->text);
    }
    free(target);
}

char *DCHAR_DestroyAndGetText(dchar *target)
{
    DCHAR_Add(target, '\0');
    target->text = (char *)realloc(target->text, target->size * sizeof(char));
    char *text = target->text;
    free(target);
    return text;
}
