#include "stack.h"

void stack_init(stack* to_init)
{
    to_init->top = NULL;
}

int stack_push(stack* target, void* data)
{
    struct _stack_elem * to_add = (struct _stack_elem *)malloc(sizeof(struct _stack_elem));
    if (to_add == NULL) return 1;

    to_add->data = data;
    to_add->next = target->top;
    target->top = to_add;

    return 0;
}

unsigned stack_get_size(stack* target)
{
    struct _stack_elem * tmp = target->top;
    unsigned number_of_elements;
    for (number_of_elements = 0;tmp != NULL;number_of_elements++) tmp = tmp->next;
    return number_of_elements;
}

void** stack_get_as_field(stack* target)
{
    unsigned number_of_elements = stack_get_size(target);

    void** data_field = (void**)malloc(sizeof(void*)*number_of_elements);
    if (data_field == NULL) return NULL;

    struct _stack_elem * tmp = target->top;
    for (unsigned i = 0;i < number_of_elements;i++)
    {
        data_field[i] = tmp->data;
        tmp = tmp->next;
    }
    return data_field;
}

void stack_free(stack* to_free)
{
    struct _stack_elem * tmp;
    while (to_free->top != NULL)
    {
        tmp = to_free->top->next;
        free(to_free->top);
        to_free->top = tmp;
    }
}