#include "daycounter.h"

#define PROCEED 0
#define WAIT 1

#define JUST_DAYS 0
#define ALL_EVENTS 1

typedef struct
{
  int wait_on_user_input;
  int event_types;
  char* days_file;
} arguments;

void niceprint(DAYCOUNT_Event** events, unsigned event_count)
{
  for (unsigned i = 0;i < event_count;i++)
  {
    if (events[i]->event_type == DAYCOUNT_EVENTTYPE_DATE)
    {
      printf("%s\n", events[i]->record->name);
      printf("Born in: %d. %d. %d\n", events[i]->record->date_day, events[i]->record->date_month, events[i]->record->date_year);
      if (events[i]->record->type == DAYCOUNT_TYPE_BIRTHDAY)
      {
        printf("Is having birthday in %d days.\n", events[i]->days_to_nearest);
        printf("%d -> %d\n", events[i]->record->years_since_date, events[i]->record->years_since_date + 1);
        printf("Curently he have %d days.\n", events[i]->record->seconds_since_day/86400);
      }
      printf("Info: %s\n", events[i]->record->info);
    }
    else if (events[i]->event_type == DAYCOUNT_EVENTTYPE_SECONDS)
    {
      printf("%s\n", events[i]->record->name);
      printf("Born in: %d. %d. %d\n", events[i]->record->date_day, events[i]->record->date_month, events[i]->record->date_year);
      printf("Is going to have interesting seconds day.\n");
      printf("In %d days.\n", events[i]->days_to_nearest);
      printf("Curently he have %d seconds.\n", events[i]->record->seconds_since_day);
    }
    else if (events[i]->event_type == DAYCOUNT_EVENTTYPE_MINUTES)
    {
      printf("%s\n", events[i]->record->name);
      printf("Born in: %d. %d. %d\n", events[i]->record->date_day, events[i]->record->date_month, events[i]->record->date_year);
      printf("Is going to have interesting minutes day.\n");
      printf("In %d days.\n", events[i]->days_to_nearest);
      printf("Curently he have %d minutes.\n", events[i]->record->seconds_since_day/60);
    }
    else if (events[i]->event_type == DAYCOUNT_EVENTTYPE_HOURS)
    {
      printf("%s\n", events[i]->record->name);
      printf("Born in: %d. %d. %d\n", events[i]->record->date_day, events[i]->record->date_month, events[i]->record->date_year);
      printf("Is going to have interesting hours day.\n");
      printf("In %d days.\n", events[i]->days_to_nearest);
      printf("Curently he have %d hours.\n", events[i]->record->seconds_since_day/3600);
    }
    else if (events[i]->event_type == DAYCOUNT_EVENTTYPE_DAYS)
    {
      printf("%s\n", events[i]->record->name);
      printf("Born in: %d. %d. %d\n", events[i]->record->date_day, events[i]->record->date_month, events[i]->record->date_year);
      printf("Is going to have interesting days day.\n");
      printf("In %d days.\n", events[i]->days_to_nearest);
      printf("Curently he have %d days.\n", events[i]->record->seconds_since_day/86400);
    }
    printf("\n");
  }
}

int main(int argc, char *argv[])
{
  if (argc < 2)
  {
    printf("Not enough arguments.\n");
    return 1;
  }

  arguments args;
  args.wait_on_user_input = PROCEED;
  args.event_types = JUST_DAYS;
  args.days_file = NULL;

  for (int i = 1;i < argc;i++)
  {
    if (strcmp(argv[i], "--wait") == 0)
    {
      args.wait_on_user_input = WAIT;
    }
    else if (strcmp(argv[i], "--event") == 0)
    {
      args.event_types = ALL_EVENTS;
    }
    else
    {
      args.days_file = argv[i];
    }
  }

  int records_count;
  unsigned event_count;
  DAYCOUNT_Record* records = DAYCOUNT_LoadFile(argv[1], &records_count);
  if (records_count == 0)
  {
    printf("No records found in: %s", argv[1]);
    return 1;
  }
  DAYCOUNT_Event** events = DAYCOUNT_GetEventsFromRecords(records, records_count, &event_count);
  
  qsort(events, event_count, sizeof(DAYCOUNT_Event*), DAYCOUNT_Comparator);
  niceprint(events, event_count);

  DAYCOUNT_FreeRecords(records, records_count);
  DAYCOUNT_FreeEvents(events, event_count);
  if (args.wait_on_user_input == WAIT) getchar();

  return 0;
}
