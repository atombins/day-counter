#ifndef DAYCOUNTER_H_INCLUDED
#define DAYCOUNTER_H_INCLUDED

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "../lib/dynamicchar.h"
#include "../lib/stack.h"

#define DAYCOUNT_TYPE_OTHER 0
#define DAYCOUNT_TYPE_BIRTHDAY 1
#define DAYCOUNT_TYPE_HOLIDAY 2

#define DAYCOUNT_EVENTTYPE_NULL 0
#define DAYCOUNT_EVENTTYPE_SECONDS 1
#define DAYCOUNT_EVENTTYPE_MINUTES 2
#define DAYCOUNT_EVENTTYPE_HOURS 3
#define DAYCOUNT_EVENTTYPE_DAYS 4
#define DAYCOUNT_EVENTTYPE_DATE 5

#define TIME_T_MAX 9223372036854775807

typedef struct
{
  unsigned type;
  char* info;
  char* name;
  int date_day;
  int date_month;
  int date_year;
  int years_since_date;
  time_t seconds_since_day;
} DAYCOUNT_Record;

typedef struct _DAYCOUNT_Event
{
  unsigned event_type;
  DAYCOUNT_Record* record;
  int days_to_nearest;
} DAYCOUNT_Event;

/**
 * @brief 
 * 
 * @param path_to_file 
 * @param count 
 * @return DAYCOUNT_Record* 
 */
DAYCOUNT_Record* DAYCOUNT_LoadFile(char* path_to_file, int* count);

/**
 * @brief 
 * 
 * @param records 
 * @param count_of_records 
 * @param count_of_events 
 * @return DAYCOUNT_Event** 
 */
DAYCOUNT_Event** DAYCOUNT_GetEventsFromRecords(DAYCOUNT_Record* records, unsigned count_of_records, unsigned *count_of_events);

/**
 * @brief Function will comompare two event structures based on nearest event.
 * 
 * @param e1 
 * @param e2 
 * @return int 
 */
int DAYCOUNT_Comparator(const void* e1, const void* e2);

/**
 * @brief Function will free all DAYCOUNT_Event field.
 * 
 * @param events Field of events
 * @param count number of records in field
 */
void DAYCOUNT_FreeEvents(DAYCOUNT_Event** events, unsigned count);

/**
 * @brief Function will free all DAYCOUNT_Records from field. Note: This could potentiali damage any DAYCOUNT_Event if it was created from any of these freed records. See DAYCOUNT_Event structure.
 * 
 * @param records 
 * @param count 
 */
void DAYCOUNT_FreeRecords(DAYCOUNT_Record* records, unsigned count);

#endif // DAYCOUNTER_H_INCLUDED
