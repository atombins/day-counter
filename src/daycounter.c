#include "daycounter.h"

/**
 * @brief Function returns number of records in file.
 * 
 * @param path_to_file path to file
 * @return unsigned number of records
 */
unsigned _DAYCOUNT_GetNumberOfRecords(char* path_to_file)
{
  char tmp;
  unsigned lines_count = 0;
  FILE* file = fopen(path_to_file, "r");
  if (file == NULL) return 0;
  while (fscanf(file, "%c", &tmp) != EOF)
  {
    if (tmp == '\n') lines_count++;
  }
  fclose(file);
  return lines_count;
}

char* _DAYCOUNT_ReadText(FILE* file)
{
  dchar* info = DCHAR_Init();
  char tmp = '0';
  while (tmp != '\"')
  {
    if (fscanf(file, "%c", &tmp) == EOF)
    {
      DCHAR_Destroy(info);
      return NULL;
    }
  }
  if (fscanf(file, "%c", &tmp) == EOF)
  {
    DCHAR_Destroy(info);
    return NULL;
  }
  while (tmp != '\"')
  {
    DCHAR_Add(info, tmp);
    if (fscanf(file, "%c", &tmp) == EOF)
    {
      DCHAR_Destroy(info);
      return NULL;
    }
  }
  return DCHAR_DestroyAndGetText(info);
}

void _DAYCOUNT_ReadDate(FILE* file, int* day, int* month, int* year)
{
  fscanf(file, "%d", day);
  fscanf(file, "%d", month);
  fscanf(file, "%d", year);
}

int _DAYCOUNT_DaysToNextInterestingNumberForSeconds(time_t time)
{
  if ((time%100000000)/86400 == 0) return 0;
  return (100000000 - (time%100000000))/86400;  
}

int _DAYCOUNT_DaysToNextInterestingNumberForMinutes(time_t time)
{
  if ((time%1000000)/1440 == 0) return 0;
  return (1000000 - (time%1000000))/1440;
}

int _DAYCOUNT_DaysToNextInterestingNumberForHours(time_t time)
{
  if ((time%100000)/24 == 0) return 0;
  return (100000 - (time%100000))/24;
}

int _DAYCOUNT_DaysToNextInterestingNumberForDays(time_t time)
{
  if (time%1000 == 0) return 0;
  return 1000 - (time%1000);
}

int _DAYCOUNT_GetType(FILE* file)
{
  char tmp[20];
  fscanf(file, "%19s", tmp);
  if (strcmp("birthday", tmp) == 0) return DAYCOUNT_TYPE_BIRTHDAY;
  if (strcmp("holiday", tmp) == 0) return DAYCOUNT_TYPE_HOLIDAY;
  return DAYCOUNT_TYPE_OTHER;
}

void _DAYCOUNT_ProcessRecord(DAYCOUNT_Record* toProccess, time_t time_now, int year_now, int month_now, int day_now, stack* target)
{
  struct tm * tmp_tm_time = (struct tm *)malloc(sizeof(struct tm));
  tmp_tm_time->tm_sec = 0;
  tmp_tm_time->tm_min = 0;
  tmp_tm_time->tm_hour = 0;
  tmp_tm_time->tm_mday = toProccess->date_day - 1;
  tmp_tm_time->tm_mon = toProccess->date_month - 1;
  tmp_tm_time->tm_year = toProccess->date_year - 1900;
  time_t proccess_time = mktime(tmp_tm_time);

  toProccess->years_since_date = year_now - tmp_tm_time->tm_year;
  if (month_now < tmp_tm_time->tm_mon) toProccess->years_since_date--;
  else if (month_now == tmp_tm_time->tm_mon && day_now < tmp_tm_time->tm_mday) toProccess->years_since_date--;

  time_t tmp_seconds = time_now - proccess_time;
  toProccess->seconds_since_day = tmp_seconds;
  DAYCOUNT_Event* event;

  switch (toProccess->type)
  {
    case DAYCOUNT_TYPE_BIRTHDAY:
      event = (DAYCOUNT_Event*)malloc(sizeof(DAYCOUNT_Event));
      event->record = toProccess;
      event->event_type = DAYCOUNT_EVENTTYPE_SECONDS;
      event->days_to_nearest = _DAYCOUNT_DaysToNextInterestingNumberForSeconds(tmp_seconds);
      stack_push(target, event);

      event = (DAYCOUNT_Event*)malloc(sizeof(DAYCOUNT_Event));
      event->record = toProccess;
      event->event_type = DAYCOUNT_EVENTTYPE_MINUTES;
      event->days_to_nearest = _DAYCOUNT_DaysToNextInterestingNumberForMinutes(tmp_seconds/60);
      stack_push(target, event);

      event = (DAYCOUNT_Event*)malloc(sizeof(DAYCOUNT_Event));
      event->record = toProccess;
      event->event_type = DAYCOUNT_EVENTTYPE_HOURS;
      event->days_to_nearest = _DAYCOUNT_DaysToNextInterestingNumberForHours(tmp_seconds/3600);
      stack_push(target, event);

      event = (DAYCOUNT_Event*)malloc(sizeof(DAYCOUNT_Event));
      event->record = toProccess;
      event->event_type = DAYCOUNT_EVENTTYPE_DAYS;
      event->days_to_nearest = _DAYCOUNT_DaysToNextInterestingNumberForDays(tmp_seconds/86400);
      stack_push(target, event);
      break;
    default:
      break;
  }

  tmp_tm_time->tm_year = year_now;
  proccess_time = mktime(tmp_tm_time);
  if (proccess_time - time_now < 0)
  {
    tmp_tm_time->tm_year = year_now;
    proccess_time = mktime(tmp_tm_time);
  }

  event = (DAYCOUNT_Event*)malloc(sizeof(DAYCOUNT_Event));
  event->record = toProccess;
  event->event_type = DAYCOUNT_EVENTTYPE_DATE;
  event->days_to_nearest = (proccess_time - time_now)/86400 + 1; // 0 is unreachable
  if (month_now + 1 == toProccess->date_month && day_now == toProccess->date_day)
  {
    event->days_to_nearest = 0;
  }
  stack_push(target, event);

  free(tmp_tm_time);
}

void _DAYCOUNT_LoadNextRecord(FILE* file, DAYCOUNT_Record* dest)
{
  // LoadData
  dest->type = _DAYCOUNT_GetType(file);
  dest->info = _DAYCOUNT_ReadText(file);
  dest->name = _DAYCOUNT_ReadText(file);
  _DAYCOUNT_ReadDate(file, &(dest->date_day), &(dest->date_month), &(dest->date_year));
}

/**
 * @brief Loads all possible records.
 * 
 * @param path_to_file 
 * @param count number of records is returned here
 * @return DAYCOUNT_Record* 
 */
DAYCOUNT_Record* DAYCOUNT_LoadFile(char* path_to_file, int* count)
{
  *count = _DAYCOUNT_GetNumberOfRecords(path_to_file);
  if (*count == 0) return NULL;
  DAYCOUNT_Record* ret = (DAYCOUNT_Record*)malloc(sizeof(DAYCOUNT_Record)*(*count));

  FILE* file = fopen(path_to_file, "r");

  for (unsigned i = 0;i < *count;i++)
  {
    // load data
    _DAYCOUNT_LoadNextRecord(file, &(ret[i]));
  }
  fclose(file);
  return ret;
}

DAYCOUNT_Event** DAYCOUNT_GetEventsFromRecords(DAYCOUNT_Record* records, unsigned count_of_records, unsigned *count_of_events)
{
  stack st;
  stack_init(&st);

  time_t currect_time, tmp_time;
  struct tm * tmp_tm_time;
  currect_time = time(NULL);
  tmp_tm_time = localtime(&currect_time);
  tmp_tm_time->tm_sec = 0;
  tmp_tm_time->tm_min = 0;
  tmp_tm_time->tm_hour = 0;
  currect_time = mktime(tmp_tm_time);
  int this_year = tmp_tm_time->tm_year;
  int this_mon = tmp_tm_time->tm_mon;
  int this_day = tmp_tm_time->tm_mday;

  for (unsigned i = 0;i < count_of_records;i++)
  {
    _DAYCOUNT_ProcessRecord(records + i, currect_time, this_year, this_mon, this_day, &st);
  }

  *count_of_events = stack_get_size(&st);
  DAYCOUNT_Event** ret = (DAYCOUNT_Event**)stack_get_as_field(&st);
  stack_free(&st);
  return ret;
}

int DAYCOUNT_Comparator(const void* e1, const void* e2)
{
  return abs((*(DAYCOUNT_Event**)e2)->days_to_nearest) - abs((*(DAYCOUNT_Event**)e1)->days_to_nearest);
}

void DAYCOUNT_FreeEvents(DAYCOUNT_Event** events, unsigned count)
{
  for (unsigned i = 0;i < count;i++)
  {
    free(events[i]);
  }
  free(events);
}

void DAYCOUNT_FreeRecords(DAYCOUNT_Record* records, unsigned count)
{
  for (unsigned i = 0;i < count;i++)
  {
    free(records[i].info);
    free(records[i].name);
  }
  free(records);
}
