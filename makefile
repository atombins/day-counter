GPP = gcc
SOURCES = $(wildcard src/*.c lib/*.c)
OBJ = $(patsubst %.c,obj/%.o,$(SOURCES))
OBJDIRS = $(dir $(OBJ))
CFLAGS = -g -lm

.PHONY: clean build

$(shell mkdir -p $(OBJDIRS))

day-counter: $(OBJ)
	$(GPP) $(CFLAGS) $^ -o $@

obj/%.o: %.c
	$(GPP) $(CFLAGS) -c -o $@ $^

clean:
	rm -rf obj
